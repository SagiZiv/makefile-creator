# README #

*The script must be on the project folder in order to function properly and comfortably.

This sctipt creates a Makefile that is than used to compile your C project.

Currently, when running the script you would be asked for the folder in which the C file you want to compile are, if you want all the files, just type period (.).

The script would find all the C files in the given folders and their dependencies.

The second thing you would need to provide is the name of the executable file.

You can write "exec" after the script name (./MakefileCreator exec) to run the Makefile after the script completed.

EXAMPLE:
We created a folder named "MyProj" on the Desktop, in the terminal we would see:
~/Desktop/MyProj$
We would write the following to run the script:
~/Desktop/MyProj$ ./MakefileCreator
Result: That terminal would print:
Enter the folder you want to compile: 

Lets say our C files are splitted between 2 folders, one named "A" and another named "B", So:

Enter the folder you want to compile: A B

* Notice that we seperate the folders names with a space!
The script then prints all the classes and their dependencies.